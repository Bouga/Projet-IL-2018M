import links from './links.json'

import { noProtectPostAsync, getAsync, postAsync, putAsync, deleteAsync } from './helpers'

class ApiServices {
    constructor() {
        let proto = links.protocole;
        let url = links.url;
        let port = links.port;
        let baseUrl = links.baseUrl;

        this.URL = proto + url + port + baseUrl;
    }
}

export default new ApiServices();
