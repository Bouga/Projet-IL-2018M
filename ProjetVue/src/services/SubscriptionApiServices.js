import links from './links.json'

import { anonymousPostAsync, getAsync, postAsync, putAsync, deleteAsync } from './helpers'

class SubscriptionApiServices {
    constructor() {
        let proto = links.protocole;
        let url = links.url;
        let port = links.port;
        let baseUrl = links.baseUrl;

        this.URL = proto + url + port + baseUrl;
    }

    /*
        Enregistre une souscription
    */
    async createSubscriptionAsync(userGuid, shopSiret) {
        return await postAsync(this.URL + "/Subscribe", {userGuid, shopSiret});
    }

    /*
        Récupère les souscriptions d'un utilisateur
        ("All" pour récupérer toutes les souscriptions de l'utilisateur, ou un siret pour une souscription qui concerne un commerce)
    */
    async getSubscriptionId(param) {
        return await postAsync(this.URL  + "/GetSubscriptions", param);
    }

    /*
        Incrémente une souscription (ajoute un point de fidélité)
    */
    async incrementSubscription(items) {
        return await postAsync(this.URL + "/IncrementSubscription", items);
    }

    /*
        Supprime une souscription
    */
    async deleteSubscriptionAsync(items) {
        // return await deleteAsync(this.URL + "/Subscription", items);
        throw {name : "NotImplementedError", message : "Not done yet, be f*cking patient !"};
    }

}

export default new SubscriptionApiServices();
