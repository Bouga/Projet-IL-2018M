import { getAsync } from './helpers'

class Geocode {
    constructor() {

    }

    // Requête pour le Geocode de l'adresse du commerce... qui sera stocké en base par la suite
    async getCoords(adress) {
        return await getAsync(
            'https://maps.googleapis.com/maps/api/geocode/json?address='
            + adress.split(' ').join('+') +
            '&key=AIzaSyAxWeeuQm8A95xdxyoDfUyllKydQoiOk7Y');
    }
}

export default new Geocode();
