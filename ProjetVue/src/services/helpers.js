async function checkErrors(resp) {
    if(resp.ok) return resp;

    let errorMsg = `ERROR ${resp.status} (${resp.statusText})`;
    let serverText = await resp.text();
    if(serverText) errorMsg = `${errorMsg}: ${serverText}`;

    var error = new Error(errorMsg);
    error.response = resp;
    throw error;
}

function toJSON(resp) {
    return resp.json()
}

// Requête non protégé par token (login et register)
export async function anonymousPostAsync(url, items) {
    var req = await fetch(url, {
        method: 'POST',
        body: JSON.stringify(items),
        headers: {
            'Content-Type': 'application/json'
        }
    });
    var reqErrorChecked = await checkErrors(req);
    return reqErrorChecked;
}

// Requête protégé par token // (PLUS MAINTENANT)

export async function getAsync(url) {
    var token = JSON.parse(localStorage.getItem('token'));

    var req = await fetch(url, {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer ' + token
        }
    });
    var reqErrorChecked = await checkErrors(req);
    return reqErrorChecked;
}

export async function postAsync(url, items) {
    var token = JSON.parse(localStorage.getItem('token'));

    var req = await fetch(url, {
        method: 'POST',
        body: JSON.stringify(items),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
    });
    var reqErrorChecked = await checkErrors(req);
    return reqErrorChecked;
}

export async function putAsync(url, items) {
    var token = JSON.parse(localStorage.getItem('token'));

    var req = await fetch(url, {
        method: 'PUT',
        body: JSON.stringify(items),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
    });
    var reqErrorChecked = await checkErrors(req);
    return reqErrorChecked;
}

export async function deleteAsync(url) {
    var token = JSON.parse(localStorage.getItem('token'));

    var req = await fetch(url, {
        method: 'DELETE',
        headers: {
            'Authorization': 'Bearer ' + token
        }
    });
    var reqErrorChecked = checkErrors(req);
    return reqErrorChecked;
}
