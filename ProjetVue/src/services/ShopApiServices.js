import links from './links.json'

import { anonymousPostAsync, getAsync, postAsync, putAsync, deleteAsync } from './helpers'

class ShopApiServices {
    constructor() {
        let proto = links.protocole;
        let url = links.url;
        let port = links.port;
        let baseUrl = links.baseUrl;

        this.URL = proto + url + port + baseUrl;
    }

    /*
        Enregistre le commerce
    */
    async createOrUpdateShopAsync(items) {
        return await postAsync(this.URL + "/CreateShop", items);
    }

    /*
        Récupère un shop par son siret ou le Guid du propriétaire
    */
    async getShopAsync(param) {
        return await postAsync(this.URL + "/GetShop", param);
    }

    /*
        Supprime un commerce
    */
    async deleteShopAsync(siret) {
        // return await deleteAsync(this.URL + "/Shop", siret);
        throw {name : "NotImplementedError", message : "Not done yet, be f*cking patient !"};
    }

}

export default new ShopApiServices();
