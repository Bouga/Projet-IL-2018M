import links from './links.json'

import { anonymousPostAsync, getAsync, postAsync, putAsync, deleteAsync } from './helpers'

class UserApiServices {
    constructor() {
        let proto = links.protocole;
        let url = links.url;
        let port = links.port;
        let baseUrl = links.baseUrl;

        this.URL = proto + url + port + baseUrl;
    }

    /*
        Inscrit l'utilisateur
    */
    async createUserAsync(items) {
        return await anonymousPostAsync(this.URL + "/CreateUser", items);
    }

    /*
        Modifie les informations d'un utilisateur
    */
    async updateUserAsync(items) {
        // return await postAsync(this.URL + "/User", items);
        throw {name : "NotImplementedError", message : "Not done yet, be f*cking patient !"};
    }

    /*
        Test une connexion utilisateur
    */
    async logInAsync(items) {
        return await anonymousPostAsync(this.URL + "/LogIn", items);
    }

    /*
        Récupère le Qr Code d'un utilisateur
    */
    async getQrCodeAsync() {
        return await postAsync(this.URL + "/GetGuid", {});
    }

    /*
        Supprime un utilisateur
    */
    async deleteUserAsync(id) {
        throw {name : "NotImplementedError", message : "Not done yet, be f*cking patient !"};
    }

}

export default new UserApiServices();
