import Vue from 'vue'
import Router from 'vue-router'

import Login from '@/Login'
import Inscription from '@/Inscription'

import ShopSettings from '@/components/shopsettings/ShopSettings'
import CardDetails from '@/components/carddetails/CardDetails'

import ProgramSubscripted from '@/components/programsubscripted/ProgramSubscripted'
import Messages from '@/components/messages/Messages'

import QrCodeScanner from '@/components/qrcode-scanner/QrCodeScanner'
import QrCode from '@/components/qrcode-scanner/QrCode'
import Scanner from '@/components/qrcode-scanner/Scanner'

import Localisation from '@/components/localisation/Localisation'
import ShopList from '@/components/localisation/ShopList'
import Map from '@/components/localisation/Map'

import ContactUs from '@/components/contactUs/ContactUs'
import Settings from '@/components/settings/Settings'
import UserInfoUpdate from '@/components/settings/UserInfoUpdate'

import About from '@/components/About'

Vue.use(Router)

export default new Router({
    routes: [
        { path: '/Login', name: 'loginRoute', component: Login },
        { path: '/Inscription', name: 'inscriptionRoute', component: Inscription },

        { path: '/ShopSettings', name: 'shopSettingsRoute', component: ShopSettings },
        { path: '/CardDetails/:id+', name: 'cardDetailsRoute', component: CardDetails },

        { path: '/ProgramSubscripted', name: 'programSubscriptedRoute', component: ProgramSubscripted },

        { path: '/Messages', name: 'messageRoute', component: Messages },

        { path: '/QrCodeScanner', name: 'qrcodeScannerRoute', component: QrCodeScanner },
        { path: '/QrCode', name: 'qrcodeRoute', component: QrCode },
        { path: '/Scanner', name: 'scannerRoute', component: Scanner },

        { path: '/Localisation', name: 'localisationRoute', component: Localisation },
        { path: '/ShopList', name: 'shopListRoute', component: ShopList },
        { path: '/Map', name: 'mapRoute', component: Map },

        { path: '/ContactUs', name: 'contactUsRoute', component: ContactUs },
        { path: '/Settings', name: 'settingsRoute', component: Settings },
        { path: '/UserInfoUpdate', name: 'userInfoUpdateRoute', component: UserInfoUpdate },

        { path: '/About', name: 'aboutRoute', component: About }
    ]
})
