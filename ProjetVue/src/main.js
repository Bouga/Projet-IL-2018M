// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueJWT from 'vuejs-jwt'

import VueQRCodeComponent from 'vue-qrcode-component'
import Icons from 'vue-icon'
import VueMaterial from 'vue-material'
import * as VueGoogleMaps from 'vue2-google-maps'
import VueSVGIcon from 'vue-svgicon'

import App from './App'
import router from './router'

import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

Vue.config.productionTip = false

Vue.component('qr-code', VueQRCodeComponent)
Vue.component('vue-icon',Icons)

Vue.use(VueMaterial)
Vue.use(VueSVGIcon)
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyAxWeeuQm8A95xdxyoDfUyllKydQoiOk7Y',
        libraries: 'places'
    }
})
Vue.use(VueJWT)

/* eslint-disable no-new */

document.addEventListener("deviceready", () => {
    console.log("LE DEVICE EST PRET");
})

new Vue({
    el: '#app',
    router,
    template: '<App/>',
    components: { App }
})
