﻿using System;
using System.Net.Http;
using System.Net.Http.Formatting;

namespace Fidelease.Models
{
    public class ResponseInfo
    {
        public static HttpContent StringResponse(string message)
        {
            return new ObjectContent(
                typeof(string),
                message,
                new JsonMediaTypeFormatter()
            );
        }

        public static ObjectContent ObjectResponse(Type type, object content)
        {
            return new ObjectContent(
                type,
                content,
                new JsonMediaTypeFormatter()
            );
        }
    }
}
