using Fidelease.DAL;
using Fidelease.DAL.Entities;
using Fidelease.DAL.Helpers;
using Fidelease.DAL.Models;
using Fidelease.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Fidelease.Functions
{
    public static class CreateShop
    {
        [FunctionName("CreateShop")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequest req, TraceWriter log)
        {
            log.Info("FUNCTION : CREATE SHOP");

            Shop shop = new Shop();

            try
            {
                shop = JsonConvert.DeserializeObject<Shop>(new StreamReader(req.Body).ReadToEnd());

                if (shop.HasEmptyProperties())
                {
                    return new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent("Les donn�es du corps de la requ�te sont invalides") };
                }
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = ResponseInfo.StringResponse("Le corps de la requ�tes est vide") };
            }

            ShopServices shopServices = new ShopServices();
            ServiceResponse response = await shopServices.CreateShop(shop);
            
            return new HttpResponseMessage(response.StatusCode) { Content = ResponseInfo.ObjectResponse(typeof(ServiceResponse), response) };
        }
    }
}
