using Fidelease.DAL;
using Fidelease.DAL.Entities;
using Fidelease.DAL.Helpers;
using Fidelease.DAL.Models;
using Fidelease.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Fidelease.Functions
{
    public static class LogIn
    {
        [FunctionName("LogIn")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequest req, TraceWriter log)
        {
            log.Info("FUNCTION LOG IN");

            UserCredentials credentials = new UserCredentials();

            try
            {
                credentials = JsonConvert.DeserializeObject<UserCredentials>(new StreamReader(req.Body).ReadToEnd());
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = ResponseInfo.StringResponse("Le corps de la requ�te est vide") };
            }

            if (credentials == null)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = ResponseInfo.StringResponse("Le corps de la requ�te est vide") };
            }

            if (String.IsNullOrWhiteSpace(credentials.Email) || String.IsNullOrWhiteSpace(credentials.Password))
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = ResponseInfo.StringResponse("Les donn�es du corps de la requ�te sont invalides") };
            }

            UserServices userServices = new UserServices();
            ServiceResponse response = await userServices.LogInUser(credentials);

            return new HttpResponseMessage(response.StatusCode) { Content = ResponseInfo.ObjectResponse(typeof(ServiceResponse), response) };
        }
    }
}
