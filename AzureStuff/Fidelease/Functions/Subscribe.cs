
//using AzureFunction.Entities;
//using AzureFunction.Helpers;
//using AzureFunction.Models;
//using Microsoft.AspNetCore.Http;
//using Microsoft.Azure.WebJobs;
//using Microsoft.Azure.WebJobs.Extensions.Http;
//using Microsoft.Azure.WebJobs.Host;
//using Microsoft.WindowsAzure.Storage.Table;
//using Newtonsoft.Json;
//using System;
//using System.IO;
//using System.Net;
//using System.Net.Http;
//using System.Threading.Tasks;

//namespace Fidelease.Functions
//{
//    public static class Subscribe
//    {
//        [FunctionName("Subscribe")]
//        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequest req, TraceWriter log)
//        {
//            log.Info("C# HTTP trigger function processed a request.");

//            Subscribing sub = new Subscribing();

//            try
//            {
//                sub = JsonConvert.DeserializeObject<Subscribing>(new StreamReader(req.Body).ReadToEnd());

//                if (sub.UserGuid == null || sub.Siret == null)
//                {
//                    return new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = ResponseInfo.StringResponse("Le corps de la requ�te n'est pas valide") };
//                }
//            }
//            catch (Exception)
//            {
//                return new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = ResponseInfo.StringResponse("Le corps de la requ�te n'est pas valide") };
//            }

//            /*
//             * V�rification de l'existance de l'utilisateur concern�
//             */
//            string userGuidString = sub.UserGuid.ToString();
//            CloudTable userTable = await TableHelper.CreateTableAsync("user");
//            TableOperation retrieveUserOperation = TableOperation.Retrieve<UserEntity>(userGuidString.Substring(0, 4), userGuidString.Substring(4));

//            TableResult userResult = await userTable.ExecuteAsync(retrieveUserOperation);
//            if (userResult.Result == null)
//            {
//                return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = ResponseInfo.StringResponse("Aucun utilisateur ne correspond � l'Id de la requ�te") };
//            }

//            /*
//             * R�cup�ration du shop concern� par la souscription
//             */
//            CloudTable shopTable = await TableHelper.CreateTableAsync("shop");

//            TableOperation retrieveShopOperation = TableOperation.Retrieve<ShopEntity>(SpecialHashCode.GetKeyFromString(sub.Siret), sub.Siret);
//            TableResult shopResult = await shopTable.ExecuteAsync(retrieveShopOperation);
//            if (shopResult.Result == null)
//            {
//                return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = ResponseInfo.StringResponse("Aucun commerce enregistr� n'a ce num�ro de Siret, la souscription ne peux pas aboutir") };
//            }

//            ShopEntity shop = (ShopEntity) shopResult.Result;
            
//            /*
//             * Enregistrement de la souscription elle m�me
//             */
//            CloudTable subscriptionTable = await TableHelper.CreateTableAsync("subscription");

//            string rowKey = sub.UserGuid.ToString() + '_' + sub.Siret;
//            SubscriptionEntity subscription = new SubscriptionEntity()
//            {
//                PointNumber = 0,
//                PointGoal = shop.PointGoal,
//                Reward = shop.Reward,
//                Shop = JsonConvert.SerializeObject(shop),

//                RowKey = rowKey,
//                PartitionKey = SpecialHashCode.GetKeyFromString(rowKey)

//            };
//            TableOperation insertOperation = TableOperation.Insert(subscription);

//            try
//            {
//                await subscriptionTable.ExecuteAsync(insertOperation);
//            }
//            catch (Exception)
//            {
//                return new HttpResponseMessage(HttpStatusCode.Conflict) { Content = ResponseInfo.StringResponse("Cette souscription existe d�j�") };
//            }

//            return new HttpResponseMessage(HttpStatusCode.OK) { Content = ResponseInfo.StringResponse("La souscription a bien �t� cr��e") };
//        }
//    }

//    public class Subscribing
//    {
//        public Guid UserGuid { get; set; }
//        public string Siret { get; set; }
//    }
//}
