using Fidelease.DAL;
using Fidelease.DAL.Entities;
using Fidelease.DAL.Models;
using Fidelease.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Fidelease.Functions
{
    public static class GetGuid
    {
        [FunctionName("GetQrCode")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequest req, TraceWriter log)
        {
            log.Info("FUNCTION GET QR CODE");

            Guid guid;

            try
            {
                guid = JsonConvert.DeserializeObject<Guid>(new StreamReader(req.Body).ReadToEnd());
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = ResponseInfo.StringResponse("Le corps de la requ�te est vide") };
            }

            UserServices userServices = new UserServices();
            ServiceResponse response = await userServices.GetUser(guid);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                UserEntity user = response.Content;
                return new HttpResponseMessage(HttpStatusCode.OK) { Content = ResponseInfo.ObjectResponse(typeof(Guid), user.QrCode) };
            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = ResponseInfo.StringResponse("L'utilisateur n'a pas de Qr Code... O_O ?") };
            }
        }
    }
}
