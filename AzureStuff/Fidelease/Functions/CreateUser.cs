using Fidelease.DAL;
using Fidelease.DAL.Entities;
using Fidelease.DAL.Models;
using Fidelease.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Fidelease.Functions
{
    public static class CreateUser
    {
        [FunctionName("CreateUser")]
        public static async Task<HttpResponseMessage> RunAsync([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)]HttpRequest req, TraceWriter log)
        {
            log.Info("FUNCTION CREATE USER");

            UserEntity user = new UserEntity();

            try
            {
                user = JsonConvert.DeserializeObject<UserEntity>(new StreamReader(req.Body).ReadToEnd());
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = ResponseInfo.StringResponse("Le corps de la requ�tes est vide") };
            }

            if (user.HasEmptyProperty())
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = ResponseInfo.StringResponse("Les donn�es du corps de la requ�te sont invalides") };
            }

            UserServices userServices = new UserServices();
            ServiceResponse response = await userServices.CreateUser(user);

            return new HttpResponseMessage(HttpStatusCode.OK) { Content = ResponseInfo.StringResponse("L'utilisateur a bien �t� cr��") };
        }
    }
}
