using Fidelease.DAL.Entities;
using Fidelease.DAL.Helpers;
using Fidelease.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Fidelease.Functions
{
    public static class GetShop
    {
        [FunctionName("GetShop")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequest req, TraceWriter log)
        {
            log.Info("FUNCTION GET SHOP");

            string key;

            try
            {
                key = JsonConvert.DeserializeObject<string>(new StreamReader(req.Body).ReadToEnd());
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = ResponseInfo.StringResponse("Le corps de la requ�tes est vide") };
            }

            CloudTable shopTable = await TableHelper.CreateTableAsync("shop");

            if (key.Equals("All"))
            {
                TableContinuationToken token = null;
                var entities = new List<ShopEntity>();
                do
                {
                    var queryResult = await shopTable.ExecuteQuerySegmentedAsync(new TableQuery<ShopEntity>(), token);
                    entities.AddRange(queryResult.Results);
                    token = queryResult.ContinuationToken;
                } while (token != null);

                List<Shop> shopList = new List<Shop>();
                foreach (ShopEntity shop in entities)
                {
                    Shop projection = new Shop()
                    {
                        Owner = JsonConvert.DeserializeObject<User>(shop.Owner),
                        Name = shop.Name,
                        PhoneNumber = shop.PhoneNumber,
                        Image = shop.Image,
                        Description = shop.Description,
                        Schedule = JsonConvert.DeserializeObject<List<Schedule>>(shop.Schedule),
                        Category = shop.Category,
                        Siret = shop.Siret,
                        Address = JsonConvert.DeserializeObject<Address>(shop.Address),
                        PointGoal = shop.PointGoal,
                        Reward = shop.Reward
                    };
                    shopList.Add(projection);
                }

                return new HttpResponseMessage(HttpStatusCode.OK) { Content = ResponseInfo.ObjectResponse(typeof(List<Shop>), shopList) };
            }
            else
            {
                string partitionKey = SpecialHashCode.GetKeyFromString(key);

                TableOperation retrieveOperation = TableOperation.Retrieve<ShopEntity>(partitionKey, key);
                TableResult retrievedResult = await shopTable.ExecuteAsync(retrieveOperation);

                if (retrievedResult.Result == null)
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = ResponseInfo.StringResponse("Aucun commerce est enregistr� avec ce num�ro de SIRET") };
                }
                else
                {
                    ShopEntity shop = (ShopEntity) retrievedResult.Result;
                    Shop shopProjection = new Shop()
                    {
                        Owner = JsonConvert.DeserializeObject<User>(shop.Owner),
                        Name = shop.Name,
                        PhoneNumber = shop.PhoneNumber,
                        Image = shop.Image,
                        Description = shop.Description,
                        Schedule = JsonConvert.DeserializeObject<List<Schedule>>(shop.Schedule),
                        Category = shop.Category,
                        Siret = shop.Siret,
                        Address = JsonConvert.DeserializeObject<Address>(shop.Address),
                        PointGoal = shop.PointGoal,
                        Reward = shop.Reward
                    };

                    return new HttpResponseMessage(HttpStatusCode.OK) { Content = ResponseInfo.ObjectResponse(typeof(Shop), shopProjection) };
                }
            }
        }
    }
}
