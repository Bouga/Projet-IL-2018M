
//using AzureFunction.Entities;
//using AzureFunction.Helpers;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.Azure.WebJobs;
//using Microsoft.Azure.WebJobs.Extensions.Http;
//using Microsoft.Azure.WebJobs.Host;
//using Microsoft.WindowsAzure.Storage.Table;
//using Newtonsoft.Json;
//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Net;
//using System.Net.Http;
//using System.Threading.Tasks;

//namespace Fidelease.Functions
//{
//    public static class IncrementSubscription
//    {
//        [FunctionName("IncrementSubscription")]
//        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequest req, TraceWriter log)
//        {
//            log.Info("FUNCTION : INCREMENT SUBSCRIPTION");

//            IncrementationId key = new IncrementationId();

//            try
//            {
//                key = JsonConvert.DeserializeObject<IncrementationId>(new StreamReader(req.Body).ReadToEnd());
//            }
//            catch (Exception)
//            {
//                return new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = ResponseInfo.StringResponse("Le corps de la requ�te n'est pas valide") };
//            }

//            /*
//             * Il faut r�cup le shop avec le Guid de l'owner, pour avoir son siret
//             */
//            CloudTable shopTable = await TableHelper.CreateTableAsync("shop");

//            /*
//             * Boucle sur tout les shops pour trouver celui dont le owner poss�de le guid de la requ�te
//             */
//            TableContinuationToken token = null;
//            var entities = new List<ShopEntity>();
//            do
//            {
//                var queryResult = await shopTable.ExecuteQuerySegmentedAsync(new TableQuery<ShopEntity>(), token);
//                entities.AddRange(queryResult.Results);
//                token = queryResult.ContinuationToken;
//            } while (token != null);

//            Shop shopWithOwnerGuid = new Shop();
//            foreach (ShopEntity shop in entities)
//            {
//                Shop shopToTest = new Shop()
//                {
//                    Owner = JsonConvert.DeserializeObject<User>(shop.Owner),
//                    Name = shop.Name,
//                    PhoneNumber = shop.PhoneNumber,
//                    Image = shop.Image,
//                    Siret = shop.Siret,
//                    Address = JsonConvert.DeserializeObject<Address>(shop.Address)
//                };
//                if (shopToTest.Owner.Id.Equals(key.OwnerGuid))
//                {
//                    shopWithOwnerGuid = shopToTest;
//                    break;
//                }
//            }

//            CloudTable subscriptionTable = await TableHelper.CreateTableAsync("subscription");

//            string rowKey = key.UserGuid.ToString() + '_' + shopWithOwnerGuid.Siret;
//            TableOperation retrieveSubscription = TableOperation.Retrieve<SubscriptionEntity>
//            (
//                SpecialHashCode.GetKeyFromString(rowKey),
//                rowKey
//            );
//            TableResult subscriptionResult = await subscriptionTable.ExecuteAsync(retrieveSubscription);

//            if (subscriptionResult.Result == null)
//            {
//                return new HttpResponseMessage(HttpStatusCode.NotFound) { Content = ResponseInfo.StringResponse("Soit le client ou le commerce n'existe pas, soit le client n'est pas souscrit au programme") };
//            }
//            else
//            {
//                SubscriptionEntity subscription = (SubscriptionEntity) subscriptionResult.Result;
//                subscription.PointNumber++;

//                TableOperation replaceOperation = TableOperation.Replace(subscription);
//                await subscriptionTable.ExecuteAsync(replaceOperation);
//            }

//            return new HttpResponseMessage(HttpStatusCode.OK) { Content = ResponseInfo.StringResponse("L'utilisateur scann� a re�u un point de fid�lit�") };
//        }
//    }

//    public class IncrementationId
//    {
//        public Guid UserGuid { get; set; }
//        public Guid OwnerGuid { get; set; }
//    }

//    public class ShopSubscription
//    {
//        public string Name { get; set; }
//        public string PhoneNumber { get; set; }
//        public string Image { get; set; }
//        public Address Address { get; set; }
//    }
//}
