﻿namespace Fidelease.DAL.Entities
{
    public class Address
    {
        public Address()
        {

        }

        public int Number { get; set; }
        public string Road { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
    }
}
