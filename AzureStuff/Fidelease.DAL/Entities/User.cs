﻿using System;

namespace Fidelease.DAL.Entities
{
    public class User
    {
        public User() { }

        public Guid Id { get; set; }
        public Guid QrCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsShopOwner { get; set; }

        public bool HasEmptyProperty()
        {
            return !(
                !String.IsNullOrWhiteSpace(FirstName) &&
                !String.IsNullOrWhiteSpace(LastName) &&
                !(BirthDate == null) && !String.IsNullOrWhiteSpace(BirthDate.ToString()) &&
                !String.IsNullOrWhiteSpace(Email) &&
                !String.IsNullOrWhiteSpace(Password)
            );
        }
    }
}
