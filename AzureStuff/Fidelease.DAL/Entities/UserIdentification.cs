﻿using Microsoft.WindowsAzure.Storage.Table;
using System;

namespace Fidelease.DAL.Entities
{
    public class UserIdentification : TableEntity
    {
        public Guid Id { get; set; }
        public byte[] Password { get; set; }
    }
}
