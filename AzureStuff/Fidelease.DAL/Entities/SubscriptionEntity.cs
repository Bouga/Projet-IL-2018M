﻿using Microsoft.WindowsAzure.Storage.Table;

namespace Fidelease.DAL.Entities
{
    public class SubscriptionEntity : TableEntity
    {
        public SubscriptionEntity()
        {
            
        }

        public int PointNumber { get; set; }
        public int PointGoal { get; set; }
        public string Reward { get; set; }
        public string Shop { get; set; }
    }
}
