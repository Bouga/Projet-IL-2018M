﻿using System;
using System.Collections.Generic;

namespace Fidelease.DAL.Entities
{
    public class Shop
    {
        public Shop() { }

        public User Owner { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Image { get; set; } // URL référence a priori, à vérifier
        public string Description { get; set; }
        public List<Schedule> Schedule { get; set; }
        public string Category { get; set; }
        public string Siret { get; set; }
        public Address Address { get; set; }
        public int PointGoal { get; set; }
        public string Reward { get; set; }

        public bool HasEmptyProperties()
        {
            return (
                Owner != null &&
                String.IsNullOrWhiteSpace(Name) &&
                String.IsNullOrWhiteSpace(PhoneNumber) &&
                String.IsNullOrWhiteSpace(Siret) &&
                String.IsNullOrWhiteSpace(Category) &&
                Address != null
            );
        }
    }
}
