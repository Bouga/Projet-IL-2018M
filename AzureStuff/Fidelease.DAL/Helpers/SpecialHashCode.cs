﻿using System;

namespace Fidelease.DAL.Helpers
{
    public class SpecialHashCode
    {
        public static String GetKeyFromString(string key)
        {
            int k = 0;
            unchecked
            {
                foreach (char c in key)
                {
                    k += c;
                }
            }
            return (k % 10000).ToString();
        }
    }
}
