﻿using Fidelease.DAL.Entities;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Fidelease.DAL.Helpers
{
    public class TokenHelper
    {
        public const string Secret = "abcdefghijklmnopqrstuvwxyz";
        // expireIn est en minute 
        
        public static string GenerateToken(UserIdentification user, UserEntity retrievedUser, int expireIn = 10)
        {
            var symmetricKey = Encoding.ASCII.GetBytes(Secret);
            var tokenHandler = new JwtSecurityTokenHandler();

            var now = DateTime.UtcNow;
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim("userId", user.Id.ToString()),
                    new Claim("firstName", retrievedUser.FirstName),
                    new Claim("lastName", retrievedUser.LastName),
                    new Claim("birthDate", retrievedUser.BirthDate.ToString())
                }),

                Expires = now.AddMinutes(Convert.ToInt32(expireIn)),

                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(symmetricKey), SecurityAlgorithms.HmacSha256Signature)
            };

            var stoken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(stoken);

            return token;
        }
    }
}
