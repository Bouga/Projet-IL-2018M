﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using System;
using System.Threading.Tasks;

namespace Fidelease.DAL.Helpers
{
    public class QueueHelper
    {
        public static CloudStorageAccount CreateStorageAccountFromConnectionString(string storageConnectionString)
        {
            CloudStorageAccount storageAccount;
            try
            {
                storageAccount = CloudStorageAccount.Parse(storageConnectionString);
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid storage account information provided. Please confirm the AccountName and AccountKey are valid in the app.config file - then restart the application.");
                throw;
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Invalid storage account information provided. Please confirm the AccountName and AccountKey are valid in the app.config file - then restart the sample.");
                Console.ReadLine();
                throw;
            }

            return storageAccount;
        }

        public static async Task<CloudQueue> CreateQueueAsync(string queueName)
        {
            string connectionString = "UseDevelopmentStorage=true;";

            // Retrieve storage account information from connection string.
            CloudStorageAccount storageAccount = CreateStorageAccountFromConnectionString(connectionString);

            // Create a queue client for interacting with the queue service
            CloudQueueClient tableClient = new CloudQueueClient(storageAccount.QueueStorageUri, storageAccount.Credentials);

            Console.WriteLine("Create a Table for the demo");

            // Create a queue with the queue client
            CloudQueue queue = tableClient.GetQueueReference(queueName);

            try
            {
                if (await queue.CreateIfNotExistsAsync())
                {
                    Console.WriteLine("Created Queue named: {0}", queueName);
                }
                else
                {
                    Console.WriteLine("Queue {0} already exists", queueName);
                }
            }
            catch (StorageException)
            {
                Console.WriteLine("If you are running with the default configuration please make sure you have started the storage emulator. Press the Windows key and type Azure Storage to select and run it from the list of applications - then restart the sample.");
                Console.ReadLine();
                throw;
            }

            Console.WriteLine();
            return queue;
        }
    }
}
