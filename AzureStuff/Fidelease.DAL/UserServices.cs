﻿using Fidelease.DAL.Entities;
using Fidelease.DAL.Helpers;
using Fidelease.DAL.Models;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Fidelease.DAL
{
    public class UserServices
    {
        public async Task<ServiceResponse> CreateUser(UserEntity user)
        {
            CloudTable identificationTable = await TableHelper.CreateTableAsync("identification");
            CloudTable userTable = await TableHelper.CreateTableAsync("user");

            PasswordHasher hasher = new PasswordHasher();

            UserIdentification userId = new UserIdentification()
            {
                PartitionKey = SpecialHashCode.GetKeyFromString(user.Email),
                RowKey = user.Email,
                Password = hasher.HashPassword(user.Password),
                Id = Guid.NewGuid()
            };

            user.PartitionKey = userId.Id.ToString().Substring(0, 4);
            user.RowKey = userId.Id.ToString().Substring(4);
            user.QrCode = Guid.NewGuid();
            user.Id = userId.Id;

            user.Password = null;

            TableOperation insertIdOperation = TableOperation.Insert(userId);
            TableOperation insertOperation = TableOperation.Insert(user);

            try
            {
                await identificationTable.ExecuteAsync(insertIdOperation);
                await userTable.ExecuteAsync(insertOperation);
            }
            catch (Exception)
            {
                return new ServiceResponse(HttpStatusCode.Conflict, "L'Email est déjà utilisé par un autre utilisateur");
            }

            return new ServiceResponse(HttpStatusCode.OK, "L'utilisateur a bien été créé");
        }

        public async Task<ServiceResponse> GetUser(Guid guid)
        {
            CloudTable table = await TableHelper.CreateTableAsync("user");

            TableOperation retrieveOperation = TableOperation.Retrieve<UserEntity>(
                guid.ToString().Substring(0, 4),
                guid.ToString().Substring(4)
            );
            TableResult retrievedResult = await table.ExecuteAsync(retrieveOperation);

            if (retrievedResult.Result == null)
            {
                return new ServiceResponse(HttpStatusCode.NotFound, "Aucun utilisateur avec cet Id n'existe.");
            }
            else
            {
                UserEntity user = (UserEntity)retrievedResult.Result;

                return new ServiceResponse(HttpStatusCode.OK, "Voici l'utilisateur...") { Content = user };
            }
        }

        public async Task<ServiceResponse> LogInUser(UserCredentials credentials)
        {
            CloudTable table = await TableHelper.CreateTableAsync("identification");

            TableOperation retrieveOperation = TableOperation.Retrieve<UserIdentification>(
                SpecialHashCode.GetKeyFromString(credentials.Email),
                credentials.Email
            );
            TableResult retrievedResult = await table.ExecuteAsync(retrieveOperation);

            if (retrievedResult.Result == null)
            {
                return new ServiceResponse(HttpStatusCode.NotFound, "L'email ou le mot de passe est incorrect");
            }
            else
            {
                UserIdentification user = (UserIdentification)retrievedResult.Result;
                PasswordHasher hasher = new PasswordHasher();
                if (hasher.VerifyHashedPassword(user.Password, credentials.Password) == PasswordVerificationResult.Failed)
                {
                    return new ServiceResponse(HttpStatusCode.NotFound, "L'email ou le mot de passe est incorrect");
                }
                else
                {
                    CloudTable userTable = await TableHelper.CreateTableAsync("user");
                    TableOperation retrieveUserOperation = TableOperation.Retrieve<UserEntity>(
                        user.Id.ToString().Substring(0, 4),
                        user.Id.ToString().Substring(4)
                    );
                    TableResult userResult = await userTable.ExecuteAsync(retrieveUserOperation);
                    UserEntity retrievedUser = (UserEntity)userResult.Result;

                    string token = TokenHelper.GenerateToken(user, retrievedUser);

                    return new ServiceResponse(HttpStatusCode.OK, "La connexion a réussi") { Content = token };
                }
            }
        }

        public async Task<ServiceResponse> DeleteUser(Guid guid)
        {
            CloudTable userTable = await TableHelper.CreateTableAsync("user");
            CloudTable identificationTable = await TableHelper.CreateTableAsync("identification");

            TableOperation retrieveUserOperation = TableOperation.Retrieve<UserEntity>(guid.ToString().Substring(0, 4), guid.ToString().Substring(4));
            TableResult retrievedUser = await userTable.ExecuteAsync(retrieveUserOperation);
            UserEntity user = (UserEntity)retrievedUser.Result;
            
            if (user != null)
            {
                TableOperation retrieveIdentificationOperation = TableOperation.Retrieve<UserIdentification>(
                    SpecialHashCode.GetKeyFromString(user.Email),
                    user.Email
                );
                TableResult retrievedIdentification = await identificationTable.ExecuteAsync(retrieveIdentificationOperation);
                UserIdentification userIdentification = (UserIdentification)retrievedIdentification.Result;

                TableOperation deleteUserOperation = TableOperation.Delete(user);
                TableOperation deleteIdentificationOperation = TableOperation.Delete(userIdentification);

                await userTable.ExecuteAsync(deleteUserOperation);
                await identificationTable.ExecuteAsync(deleteIdentificationOperation);

                return new ServiceResponse(HttpStatusCode.OK, "L'utilisateur a bien été supprimé");
            }
            else
            {
                return new ServiceResponse(HttpStatusCode.NotFound, "Cet utilisateur n'existe pas");
            }
        }
    }
}
