﻿using Fidelease.DAL.Entities;
using Fidelease.DAL.Helpers;
using Fidelease.DAL.Models;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Fidelease.DAL
{
    public class ShopServices
    {
        public async Task<ServiceResponse> CreateShop(Shop shop)
        {
            CloudTable table = await TableHelper.CreateTableAsync("shop");

            ShopEntity shopEntity = new ShopEntity()
            {
                Owner = JsonConvert.SerializeObject(shop.Owner),
                Name = shop.Name,
                PhoneNumber = shop.PhoneNumber,
                Image = shop.Image,
                Description = shop.Description,
                Schedule = JsonConvert.SerializeObject(shop.Schedule),
                Category = shop.Category,
                Siret = shop.Siret,
                Address = JsonConvert.SerializeObject(shop.Address),
                PointGoal = shop.PointGoal,
                Reward = shop.Reward,

                RowKey = shop.Siret,
                PartitionKey = SpecialHashCode.GetKeyFromString(shop.Siret)
            };
            TableOperation insertOperation = TableOperation.InsertOrMerge(shopEntity);

            try
            {
                await table.ExecuteAsync(insertOperation);
            }
            catch (Exception)
            {
                return new ServiceResponse(HttpStatusCode.Conflict, "Un commerce avec ce numéro de Siret est déjà enregistré");
            }

            return new ServiceResponse(HttpStatusCode.OK, "Le commerce a bien étét créé") { Content = shop.Siret };
        }
    }
}
