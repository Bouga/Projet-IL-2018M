﻿using System.Net;

namespace Fidelease.DAL.Models
{
    public class ServiceResponse
    {
        public HttpStatusCode StatusCode { get; set; }
        public string Message { get; set; }
        public dynamic Content { get; set; }

        public ServiceResponse(HttpStatusCode statusCode, string message)
        {
            StatusCode = statusCode;
            Message = message;
        }
    }
}