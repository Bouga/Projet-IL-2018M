using Fidelease.DAL;
using Fidelease.DAL.Entities;
using Fidelease.DAL.Models;
using NUnit.Framework;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Fidelease.Tests
{
    [TestFixture]
    public class UserTests
    {
        private Random _random = new Random();

        [Test]
        public async Task CreateUserTest()
        {
            UserServices userServices = new UserServices();

            /*
             * Cr�ation d'un utilisateur
             */
            UserEntity user = new UserEntity
            {
                FirstName = "FirstName-" + RandomString(10),
                LastName = "LastName-" + RandomString(10),
                BirthDate = RandomDateTime(),
                Email = RandomString(6) + "@gmail.com",
                Password = RandomString(16),
                IsShopOwner = _random.Next(2) % 2 == 0 ? true : false
            };

            // Pour sauvegarder le mot de passe (il est effac� � la cr�ation de l'user)
            string password = user.Password;

            ServiceResponse createUserResponse = await userServices.CreateUser(user);
            Assert.That(createUserResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));

            /*
             * Essaie d'une connexion utilisateur
             */
            UserCredentials credentials = new UserCredentials()
            {
                Email = user.Email,
                Password = password
            };
            ServiceResponse logInUserResponse = await userServices.LogInUser(credentials);
            Assert.That(logInUserResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));

            /*
             * R�cup�ration de l'utilisateur
             */
            ServiceResponse getUserResponse = await userServices.GetUser(user.Id);

            UserEntity userToTest = getUserResponse.Content;

            Assert.That(getUserResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(userToTest.Id, Is.EqualTo(user.Id));

            /*
             * Suppression de l'utilisateur
             */
            ServiceResponse deleteUserResponse = await userServices.DeleteUser(user.Id);

            Assert.That(deleteUserResponse.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        }

        private string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[_random.Next(s.Length)]).ToArray());
        }

        private DateTime RandomDateTime()
        {
            DateTime start = new DateTime(1995, 1, 1);
            int range = (DateTime.Today - start).Days;
            return start.AddDays(_random.Next(range));
        }
    }
}
